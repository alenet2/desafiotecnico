## Visão Geral

API "Desafio Técnico" desenvolvida em Node.js com Express. Esta API auxilia no processo de reserva de salas. O usuário pode cadastrar salas e cadastrar uma agenda com a disponibilidade das salas.

Está API já está configurada com uma base de dados com alguns registros de teste.

O banco de dados utilizado nesta API é o PostgreSQL. A API utiliza autenticaão com JWT com o framework passport.js. As senhas dos usuários são criptografados com a biblioteca bcrypt.

Para a modelagem do banco de dados foi utilizado a biblioteca Sequelize usando migrations.

## Estrutura das pastas

	/auth/auth.js // Faz a autenticação buscando usuário no BD, comprarando a senha criptografada com o bcrypt e gerando o token com JWT
	/config/config.js // Arquivo que contem a chave do JWTsecret. Podendo ser alterado para outra chave 
	/config/config.json // arquivo Json que contém as configurações do BD postgreSql como username, password, host, database. Por padrão aAPI utiliza os dados do "development". Já está configurada com uma Base de Dados real

	/migrations/20220216115816-create-init.js // Arquivo migrations do Sequelize, respónsavel por subir as tabelas do banco de dados 

	/models/index.js  // Arquivo de configuração do Sequelize que carrega os models das tabelas
	/models/agenda.js // model da tabela agenda gerado pelo sequelize
	/models/image.js // model da tabela agenda gerado pelo sequelize
	/models/sala.js // model da tabela agenda gerado pelo sequelize
	/models/user.js // model da tabela agenda gerado pelo sequelize

	/routes/routes.js // arquivo que contém as rotas
 	/routes/secure-routes.js // rotas autorizadas após autenticação. JWt
 	/seeders/

	/app.js // Arquivo raiz da API. Rode este arquivo com o comando nodemon app.js ou node app.js

---

## Instalar dependências
    npm install


## Rodar o Server com nodemon. Padrão porta 3000
Acesse a pasta do projeto e na raiz digite o seguinte comando<br>

	nodemon app.js

## Rodar o server sem nodemon: Porta Padrão 3000
 
	node app.js


## Opcional: Subir as tabelas no BD
 Caso queira subir as tabelas em outra base de dados. Crie uma base de dados no PostgreSQL e altere as informações do BD no arquivo config/config.json - "development". As tabelas estão vazias

## Comando para subir as tabelas no BD PostgreSQL (Sequelize CLI - migrate) - OPCIONAL. 
	npx sequelize-cli db:migrate

## Porta do Servidor: 3000
	http://localhost:3000

## Rotas. Utilize o POSTMAN para testar. 

	GET / - retorna status 200 e um corpo de reposta em branco; 
	exemplo: http://localhost:3000
	
	GET /room?date=2021-05-01 - retorna uma lista de salas com disponibilidade para o dia específico
	exemplo: http://localhost:3000/room?date=2022-02-17
	
 	GET /room/:id – retorna os detalhes de uma sala específica.;
 	exemplo: http://localhost:3000/room/1

 	POST /auth - autenticação do usuário (retorna 200 com um corpo que contém o token jwt );
 	exemplo: http://localhost:300/auth
 	Informe:
 	email: desafio@desafio.com
 	password: 1234
    
    Retorno: Será gerado o token para autenticação das rotas seguras.

	GET /schedule/:roomId – obter as reservas disponíveis para a uma sala;
 	exemplo: http://localhost:300/schedule/1

 	POST /schedule - realiza a reserva da agenda;
 	exemplo: http://localhost:300/schedule
 	informe via Json os seguintes campos
 	  Exemplo:
 		{
            "data": "2022-02-17",
            "periodo": "Tarde",
            "status": "Disponível",
            "salaId": 2,            
        }    
        
        
## Envie o token gerado em http://localhost:3000/auth no parâmetro Query chamado secret_token

Query
Key: secret_token   
Value: [token gerado]