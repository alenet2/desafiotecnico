const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../config/config');

const router = express.Router();

//Rotas


// Retorna status 200 e um corpo de reposta em branco
router.get('/', async (req, res)=>{
  try{
    res.json();
  }catch(error){
    res.json({error: error});

  }

});



// Autenticação do usuário (retorna 200 com um corpo que contém o token jwt );
router.post('/auth', async (req, res, next) => {
  passport.authenticate(
    'login',
    async (err, user, info) => {
      try {
        if (err || !user) {
          const error = new Error('Ocorreu um erro');

          return next(error);
        }

        req.login(
          user,
          { session: false },
          async (error) => {
            if (error) return next(error);
            
            const body = { id: user.id, email: user.email };
            const token = jwt.sign({ user: body }, config.jwtSecret,{
              expiresIn: 300 // expira em 5min

            });
            return res.json({ token });
          }
        );
      } catch (error) {
        return next(error);
      }
    }
  )(req, res, next);
  
});

router.post('/create', async (req, res, next) => {
    bcrypt.genSalt(10, function(err, salt) {
        let password = req.body.password;
        bcrypt.hash(password, salt, function(err, hash) {
   
          _user.create({
              email: req.body.email,
              password: hash
          });
          res.json({success: true, message: 'Usuário criado!'});
        });
      });
});
router.post('/logout', function(req, res) {
  res.json({ auth: false, token: null });
})

module.exports = router;

