const express = require('express');
const router = express.Router();

const models = require('../models');
let agenda = models.Agenda;
let sala = models.Sala;
let img = models.Image;


//  Retorna uma lista de salas com disponibilidade para o dia específico 
router.get('/room', async (req, res) =>{
  try{
    let response = await agenda.findAll({
        where: {data: req.query.date, status: "Disponível"},
        attributes: {exclude: ['createdAt', 'updatedAt']},
        include:{
            model: sala,
            attributes: {exclude: ['createdAt', 'updatedAt']},             
        }              
    });
    if(response == ""){
      res.json({erro: "Nenhuma sala disponível foi encontrada!"});  
    }
    res.json({response: response});

  }catch(error){
    res.json({error: error});
  }     
  
});

// Retorna os detalhes de uma sala específica
router.get('/room/:id', async (req, res) =>{
  try{
    let response = await sala.findByPk(req.params.id,{
      attributes: {exclude: ['createdAt', 'updatedAt']},                     
    });
    let respImg = await img.findAll({
      where: {salaId: req.params.id},
      attributes: {exclude: ['createdAt', 'updatedAt', 'salaId']}
    });
    if(!response){
      res.json({error: "Registro não encontrado!"});     
    }
    res.json({response: response, respImg});

  }catch(error){
    res.json({error: error});

  }     
});

// obter as reservas disponíveis para a uma sala
router.get('/schedule/:roomId', async (req, res) =>{
  try{
    let response = await agenda.findAll({
      where: {salaId: req.params.roomId, status: "Disponível"},
      attributes: {exclude: ['createdAt', 'updatedAt', 'userId', 'salaId']}, 
                  
    });
    if(!response){
      res.json({error: "Nenhum registro encontrado!"});
    }
    res.json({response: response});

  }catch(error){
    res.json({error: error});
  }      
});


// realiza a reserva da agenda
router.post('/schedule', async (req, res, next) =>{
  try{
    await agenda.create({
      data: req.body.data,
      periodo: req.body.periodo,
      status: req.body.status,
      salaId: req.body.salaId,
      userId: req.user.id
    });
    res.json({mensagem: "Agenda criada com sucesso!"});
    
  }catch(error){
    res.json({error: error});
  }     

});


router.get('/perfil', async (req, res, next) => {
    res.json({
      message: 'Você está logado em uma rota segura',
      user: req.user,
      token: req.query.secret_token
    })
  }
);


module.exports = router;
