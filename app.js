const express = require('express');
//const cors = require('cors');
const bodyParser = require('body-parser');
const passport = require('passport');

require('./auth/auth');
const routes = require('./routes/routes');
const secureRoute = require('./routes/secure-routes');

const app = express();
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));

//let user = models.User;

//Rotas que não precisam de autenticação
app.use('', routes);

// Rota segura
app.use('/', passport.authenticate('jwt', { session: false }), secureRoute);


// Handle errors.
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({ error: err });
});

let port = process.env.PORT || 3000;
app.listen(port,(req,res)=>{
    console.log('Servidor "Desafio Técnico - Backend Developer – NodeJs" Rodando');
});