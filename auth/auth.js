const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const models = require('../models');
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const bcrypt = require('bcryptjs');
const config = require('../config/config');

let userModel = models.User;

passport.use(
    'signup',
    new localStrategy(
      {
        usernameField: 'email',
        passwordField: 'password'
      },
      async (email, password, done) => {
        try {
          const user = await userModel.create({ email, password });  
          return done(null, user);
        } catch (error) {
          done(error);
        }
      }
    )
  );
  

passport.use(
    'login',
    new localStrategy(
      {
        usernameField: 'email',
        passwordField: 'password'
      },
      async (email, password, done) => {
        try {
          const user = await userModel.findOne({
                where:{
                    email: email,                   
                },
            });
        
        if (!user) {
            return done(null, false, { message: 'Usuário não encontrado' });
        }
         
        // Compara se a senha digitada é igual a senha criptografada no BD
        const validate = bcrypt.compareSync(password, user.password); 
         
        if (!validate) {
            return done(null, false, { message: 'Usuário ou senha inválido' });
          
        }  
          return done(null, user, { message: 'Logado com sucesso!' });
          
        } catch (error) {
          return done(error);
        }
      }
    )
  );
  passport.use(
    'jwt',
    new JWTstrategy(
      {
        secretOrKey: config.jwtSecret,
        jwtFromRequest: ExtractJWT.fromUrlQueryParameter('secret_token')
      },
      async (token, done) => {
        try {
          return done(null, token.user);
        } catch (error) {
          done(error);
        }
      }
    )
  );