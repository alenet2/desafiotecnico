'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Agenda extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.userId = this.belongsTo(models.User, {
        foreignKey: 'userId',
      });
      this.salaId = this.belongsTo(models.Sala, {
        foreignKey: 'salaId',
      });
     // Agenda.hasMany(models.Sala);
     // Agenda.hasMany(models.User);
    }
  }
  Agenda.init({
    data: DataTypes.DATE,
    periodo: DataTypes.STRING,
    status: DataTypes.STRING,
    userId: DataTypes.INTEGER,
    salaId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Agenda',
  });
  return Agenda;
};