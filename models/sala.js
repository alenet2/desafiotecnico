'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Sala extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      //Sala.belongsTo(models.Agenda);
      //Sala.hasMany(models.Image);
     

    }
  }
  Sala.init({
    nome: DataTypes.STRING,
    descricao: DataTypes.STRING,
    logradouro: DataTypes.STRING,
    numero: DataTypes.STRING,
    complemento: DataTypes.STRING,
    bairro: DataTypes.STRING,
    cidade: DataTypes.STRING,
    estado: DataTypes.STRING,
    pais: DataTypes.STRING,
    cep: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Sala',
  });
  return Sala;
};